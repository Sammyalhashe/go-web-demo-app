package main

import (
	"fmt"
	"html/template"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/julienschmidt/httprouter"
	"github.com/rs/cors"
)

func handlerFunc(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")
	path := r.URL.Path
	if path == "/" {
		fmt.Fprint(w, "<h1>Hello go Web</h1>")
	} else if path == "/support" {
		fmt.Fprint(w, "To get in touch, please click <a href='mailto:sammyalhashemi1@gmail.com'>here</a>")
	} else {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprint(w, "We could not find what you were looking for :(")
	}
}

func index(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	fmt.Fprint(w, "<h1>Index Page</h1>")
}

func greetName(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	fmt.Fprintf(w, "<h1>Hello %s!</h1>", ps.ByName("name"))
}

func greetGorilla(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	t, _ := template.ParseFiles("greetGorilla.html")
	t.Execute(w, vars)
	// fmt.Fprintf(w, "<h1>Hello %s!</h1>", vars["name"])
}

func main() {
	// router := httprouter.New()
	// routes using julienschmidt's router
	// router.GET("/", index)
	// router.GET("/:name", greetName)

	// routes using gorilla router
	r := mux.NewRouter()
	r.HandleFunc("/", handlerFunc)
	r.HandleFunc("/{name}", greetGorilla)

	// this was the default net/http router
	// http.HandleFunc("/", handlerFunc)
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("assets/"))))
	handler := cors.Default().Handler(r)
	srv := &http.Server{
		Handler:      handler,
		Addr:         "127.0.0.1:3001",
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	srv.ListenAndServe()
}
